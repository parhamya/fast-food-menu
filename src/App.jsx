import { useEffect, useState } from 'react'
import './App.css'
import Header from './Header/header'
import CategoryList from './CategoryList/categoryList'
import axios from './axios'
import FastFoodList from './FastFoodList/fastFoodList'
import Loading from './Loading/loading'
import SearchBar from './SerachBar/searchBar'


function App() {
  const [loading,setLoading] =useState(true);
  const [fastFoodItems,setFastFoods]=useState([]);

  const fetchData=async(categoryId =null)=>{
    // setLoading(true);
    const response = await axios.get(`/FastFood/list/${categoryId ? "?categoryId=" + categoryId : ""}`);
    setLoading(false);
    setFastFoods(response.data);
  }

  useEffect ( ()=>{
   
    fetchData();
  }),[]

  const filterItems =(categoryId)=>{
    fetchData(categoryId)
  }

  const serachItems =async(term)=>{
    setLoading(true)
    const response = await axios.get(`/FastFood/search/${term ? "?term=" + term : ""}`)
    setLoading(false)
    setFastFoods(response.data)
  }
  const renderContent=()=>{
    if(loading){
       return <Loading theme="dark"/>;
    } 
    return <FastFoodList fastFoodItems={fastFoodItems}></FastFoodList>;
  }

  return (
      <div className='wrapper bg-faded-dark'>
        <Header></Header>
        <CategoryList filterItems={filterItems}>
          <SearchBar serachItems={serachItems}/>
        </CategoryList>
        <div className='container mt-4'>
          {renderContent()}
        </div>


      </div>
  ) 
}

export default App
